-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("api")
include("wet")
include("patterns")
include("node")
include("register")
include("stylus")
include("hints")

-- LUALOCALS < ---------------------------------------------------------
local nodecore
    = nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.register_hint("navigate by touch in darkness", "craft:scaling light")
nodecore.register_hint("scale a wall", "scaling dy=0")
nodecore.register_hint("scale an overhang", "scaling dy=1")

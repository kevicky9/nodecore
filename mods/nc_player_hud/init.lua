-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("setup")
include("breath")
include("hotbar")
include("touchtip")
include("pretrans")
include("cheats")
include("hints")
